export const isAuthenticated = () => {
  let data = localStorage.getItem("headers");
  
  if (!data) {return};
  
  if (data.access_token !== "") {
    return true;
   }
};