import React from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import { isAuthenticated } from "./auth";

import Login from "../view/templates/login";
import Home from "../view/templates/home";

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route 
    { ...rest } 
    render={props =>
      isAuthenticated() ? (
        <Component { ...props } />
        ) : (
        <Redirect to={{ pathname: "/", state: {from: props.location}}} />
    )}
  />
);

const Routes = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Login} />
        <PrivateRoute path="/home" component={Home} />
      </Switch>
    </BrowserRouter>
  )
}

export default Routes;