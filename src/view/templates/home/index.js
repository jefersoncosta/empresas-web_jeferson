import React from "react";

import { Content } from "../../../style/components/contents"
import { H2 } from "../../../style/components/text";

import Header from "../../components/functional/header";

const Home = () => (
  <div>
    <Header/>
    <Content >
      <H2>Clique na busca para iniciar.</H2>
    </Content>
  </div>
)

export default Home;