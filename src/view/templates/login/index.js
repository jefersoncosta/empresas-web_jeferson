import React from "react";

import { Content, Img } from "../../../style/components/contents"
import { H1, P } from "../../../style/components/text";
import logo from "../../../img/logo-home.png";
import Form from "../../components/functional/form";

const Login = () => (
  <Content>
    <div>
      <Img src={logo} alt="ioasys" width="295"/>
      <H1>BEM-VINDO AO <br/> EMPRESAS</H1>
      <P>Lorem ipsum dolor sit amet, contetur <br/>adipiscing elit. Nunc accumsan.</P>
      <Form />
    </div>
  </Content>
)

export default Login;