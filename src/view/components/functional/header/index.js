import React, { useState } from "react";

import { FormContent, Label, InputSearch, Icon } from "../../../../style/components/form";
import { ContentFluid, ContentHeader } from "../../../../style/components/contents";
import { Margin } from "../../../../style/components/spacing";

import search from "../../../../img/ic-search.png"
import close from "../../../../img/ic-close.png";
import logo from "../../../../img/logo-nav.png"


const Header = () => {
  const [ width, setWidth ] = useState("130px");

  if(width == "90%") {
    return (
      <ContentHeader>
        <ContentFluid>
          <FormContent width={width} height="100%">
            <Label margin="auto auto 22px">
              <Icon icon={search} left="0" onClick={() => setWidth("100%")} />
              <Icon icon={close} right="0" onClick={() => setWidth("130px")}/>
              <InputSearch placeholder="Pesquisar"/>
            </Label>
        </FormContent>
        </ContentFluid>
      </ContentHeader>
    )
  } else {
    return (
      <ContentHeader>
        <ContentFluid>
          <Margin margin="auto">
            <h1>
              <img src={logo} width="234" slt="ioasys" />
            </h1>
          </Margin>
          <FormContent width={width} height="100%">
            <Label margin="auto auto 22px">
              <Icon icon={search} left="0" onClick={() => setWidth("90%")} />
            </Label>
          </FormContent>
        </ContentFluid>
      </ContentHeader>
    )
  }
}

export default Header;