import React, { Component } from "react";
import axios from "axios";

import { FormContent, Label, Input, Placeholder, Border, Button } from "../../../../style/components/form";
import { Margin, Padding } from "../../../../style/components/spacing";
import email from "../../../../img/ic-email.png";
import cadeado from "../../../../img/ic-cadeado.png";


class Form extends Component {
  constructor(props) {
    super(props);

    this.state = {
        Email: "",
        password: ""
    }
  }

  login = () => {
    let { Email, password } = this.state;

    let data = {
      email: Email,
      password: password 
    }

    let jsonData = JSON.stringify(data);

    axios({
      method: 'post',
      url: 'http://empresas.ioasys.com.br/api/v1/users/auth/sign_in',
      data: jsonData,
      headers: {
        'content-type': 'application/json'
      }
    }).then(resp => localStorage.setItem("headers", JSON.stringify(resp.headers)))
      .then(resp => window.location.href = "/home");

  }

  setMail = event => {
    this.setState({ ...this.state, Email: event.target.value })
  }
  setPass = event => {
    this.setState({ ...this.state, password: event.target.value })
  }
  render() {
    let { Email, password } = this.state;
    return(
      <FormContent width="358px">
        <Margin margin="auto 0 33px">
          <Label icon={email}>
            <Input type="email" 
              onChange={this.setMail}
              value={Email}
              id="email" 
              placeholder="&nbsp;"
            />
            <Placeholder>E-mail</Placeholder>
            <Border />
          </Label>
        </Margin>

        <Margin margin="auto 0 46px">
          <Label icon={cadeado}>
            <Input type="password"
              onChange={this.setPass}
              value={password} 
              id="pass" 
              placeholder="&nbsp;"
            />
            <Placeholder>Senha</Placeholder>
            <Border />
          </Label>
        </Margin>

        <Padding padding="0 16px">
          <Button onClick={this.login}>Entrar</Button>
        </Padding>
      </FormContent>
    )
  }
}

export default Form;