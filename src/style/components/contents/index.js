import styled from "styled-components";

export const ContentHeader = styled.header`
  position: fixed;
  width: 100%;
  display: flex;
  height: 151px;
  background: rgb(238,76,119);
  background: linear-gradient(173deg, rgba(238,76,119,1) 99%, rgba(13,4,48,1) 100%);
  transition: all .15s ease;
  .nav {
    width: 100%;
    display: ${props => props.display};
  }

  .logo {
    margin: auto;
  }
`
export const Content = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  align-content: center;
  justify-content: center;
  align-items: center;
  text-align: center;
  padding: 5px;
`

export const ContentFluid = styled.div`
  width: 100%;
  display: flex;
  align-content: center;
  justify-content: center;
  align-items: center;
  text-align: center;
  padding: 5px;
`

export const Img = styled.img`
  margin-bottom: 67px;
`