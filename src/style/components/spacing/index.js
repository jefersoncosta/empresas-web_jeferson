import styled from "styled-components";

export const Margin = styled.div`
  width: 100%;
  margin: ${props => props.margin};
`

export const Padding = styled.div`
  width: 100%;
  padding: ${props => props.padding};
`