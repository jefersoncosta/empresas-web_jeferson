import styled from "styled-components";

export const H1 = styled.h2`
  margin-bottom: 20.5px;
  font-size: 24px;
  font-weight: bold;
  color: #383743;
`
export const H2 = styled.h2`
  font-size: 32px;
  line-height: 1.22;
  letter-spacing: -0.45px;
  color: #383743;
`
export const P = styled.p`
  margin-bottom: 47px;
  font-size: 18px;
  line-height: 1.44;
  letter-spacing: -0.25px;
  color: #383743;
`