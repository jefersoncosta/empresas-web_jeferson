import styled from "styled-components";

export const FormContent = styled.div`
  width: ${props => props.width};
  height: ${props => props.height};
  display: flex;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
`

export const Label = styled.label`
  position: relative;
  margin: ${props => props.margin};
  width: 100%;
  ::before {
    content: "";
    position: absolute;
    top: 16px;
    width: 26px;
    height: 26px;
    background-image: url(${props => props.icon});
    background-size: 26px 26px;
    background-repeat: no-repeat;
  }
`

export const Placeholder = styled.span`
  position: absolute;
  top: 16px;
  left: 38px;
  font-size: 16px;
  color: #9098A9;
  font-weight: 500;
  transform-origin: 0 0;
  transition: all .2s ease;
`
export const Border = styled.span`
  position: absolute;
  bottom: 0;
  left: 0;
  height: 2px;
  width: 100%;
  background: #ee4c77;
  transform: scaleX(0);
  transform-origin: 0 0;
  transition: all .15s ease;
`
export const Input = styled.input`
  -webkit-appearance: none;
  width: 100%;
  border: 0;
  font-family: inherit;
  padding: 12px 0 2px 30px;
  height: 48px;
  font-size: 16px;
  font-weight: 500;
  border-bottom: 2px solid #C8CCD4;
  background: none;
  border-radius: 0;
  color: #223254;
  /* transition: all .15s ease; */
  :hover {
    background: rgba(#223254,.03);
  }
  :not(:placeholder-shown) {
    + span {
      color: #5A667F;
      transform: translateY(-26px) scale(.75);
    }
  }
  :focus {
    background: none;
    outline: none;
    + ${Placeholder} {
      color: #ee4c77;
      transform: translateY(-26px) scale(.75);
      
      + ${Border} {
        transform: scaleX(1);
      }
    }
  }
  
`
export const InputSearch = styled.input`
  -webkit-appearance: none;
  width: 100%;
  border: 0;
  padding: 12px 0 20px 71px;
  height: 48px;
  font-size: 34px;
  border-bottom: 1px solid #ffffff;
  background: none;
  border-radius: 0;
  color: #ffffff;

  :focus {
    outline: none;
  }

  ::-webkit-input-placeholder,
  :-moz-placeholder,
  ::-moz-placeholder,
  :-ms-input-placeholder {  
   color: #991237;  
   font-size: 34px;
  }
`

export const Button = styled.button`
  padding: 14px 127px;
  border-radius: 3.6px;
  border: none;
  background-color: #57bbbc;  
  font-size: 18px;
  font-weight: bold;
  text-align: center;
  color: #ffffff;

  :hover {
    background-color: #499da6;
  }
`

export const Icon = styled.label`
  position: absolute;
  width: 60px;
  height: 60px;
  left: ${props => props.left};
  right: ${props => props.right};
  bottom: 12px;
  background-image: url(${props => props.icon});
  background-size: 60px 60px;
  cursor: pointer;
`